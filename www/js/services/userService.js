angular.module('app')

  .factory('UserService', function($auth, $ionicPopup) {

    var authenticate = function() {
      return $auth.authenticate('bitbucket');
    };

    var logout = function() {
      $auth.logout();
    };

    return {
      authenticate: authenticate,
      logout: logout
    };
  });
