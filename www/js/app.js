angular.module('app', ['ionic', 'satellizer'])

  .run(function($ionicPlatform) {
    $ionicPlatform.ready(function() {
      // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
      // for form inputs)
      if (window.cordova && window.cordova.plugins.Keyboard) {
        cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
        cordova.plugins.Keyboard.disableScroll(true);

      }
      if (window.StatusBar) {
        // org.apache.cordova.statusbar required
        StatusBar.styleDefault();
      }
    });
  })


  .config(function($authProvider) {
    $authProvider.bitbucket({
      clientId: '8szz9MTGF49EJsGqnR',
      url: 'http://localhost:8100/',
      authorizationEndpoint: 'https://bitbucket.org/site/oauth2/authorize',
      redirectUri: 'http://localhost:8100/',
      optionalUrlParams: ['scope'],
      scope: ['email'],
      scopeDelimiter: ' ',
      type: '2.0',
      responseType: 'token',
      popupOptions: {
        location: 'no',
        toolbar: 'yes',
        width: window.screen.width,
        height: window.screen.height
      }
    });
  })

  .config(function($stateProvider, $urlRouterProvider) {
    $stateProvider

      .state('main', {
        url: '/main',
        abstract: true,
        templateUrl: 'templates/menu.html',
        controller: 'MainCtrl'
      })

      .state('main.search', {
        url: '/search',
        views: {
          'menuContent': {
            templateUrl: 'templates/search.html'
          }
        }
      })

      .state('main.browse', {
        url: '/browse',
        views: {
          'menuContent': {
            templateUrl: 'templates/browse.html'
          }
        }
      })

      .state('main.repositories', {
        url: '/repositories',
        views: {
          'menuContent': {
            templateUrl: 'templates/repositories.html',
            controller: 'RepositoriesCtrl'
          }
        }
      })

      .state('main.user', {
        url: '/user',
        views: {
          'menuContent': {
            templateUrl: 'templates/user.html',
            controller: 'UserCtrl'
          }
        }
      })

      .state('repository', {
        url: '/repository',
        abstract: true,
        templateUrl: 'templates/repository-menu.html',
        controller: 'RepositoryCtrl'
      })

      .state('repository.details', {
        url: '/repository/details/:owner/:slug',
        views: {
          'menuContent': {
            templateUrl: 'templates/repository-details.html',
            controller: 'RepositoryDetailsCtrl'
          }
        }
      });
    // if none of the above states are matched, use this as the fallback
    $urlRouterProvider.otherwise('/main/repositories');
  });
