angular.module('app')

  .controller('RepositoriesCtrl', function($scope, $ionicPopup, ApiService) {
    ApiService.getRepos().then(
      function success(data) {
        $scope.repos = data;
      },
      function error(response) {
        $ionicPopup.alert({
          title: 'Error',
          content: response.data ? response.data || response.data.message : response
        });
      }
    );
  });
