angular.module('app')

  .controller('MainCtrl', function($scope, $state, $ionicModal, $timeout, $auth, $http, $ionicPopup, UserService, ApiService) {

    // Form data for the login modal
    $scope.loginData = {};

    // Create the login modal that we will use later
    $ionicModal.fromTemplateUrl('templates/login.html', {
      scope: $scope
    }).then(function(modal) {
      $scope.modal = modal;
    });

    // Triggered in the login modal to close it
    $scope.closeLogin = function() {
      $scope.modal.hide();
    };

    // Open the login modal
    $scope.login = function() {
      UserService.authenticate()
        .then(function() {
          $ionicPopup.alert({
            title: 'Success',
            content: 'You have successfully logged in!'
          });
        })
        .catch(function(response) {
          $ionicPopup.alert({
            title: 'Error',
            content: response.data ? response.data || response.data.message : response
          });
        });
    };

    $scope.logout = function() {
      UserService.logout();
    };

    $scope.openUser = function() {
      $state.go("main.user");
    };

    $scope.openRepository = function() {
      $state.go("repository.details");
    };
  });
